# AIRASIA TECHNICAL ASSIGNMENT

Please use node v16.18.1

## Installation
```
npm ci
```

## Running the app
In the case you want to change database, let follow the `.env.example` and put it in the `.env` file. The config will be auto load to environment variable
```
npm run start:dev
```

## Test
```
npm run test:cov
```

## How to run by docker.
Ensure you have docker compose in computer.

```
docker-compose up -d
```

## Default endpoint
### Document
http://localhost:3304/docs/assignment#/
## API
http://localhost:3304/api/assignment/
