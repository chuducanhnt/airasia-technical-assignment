import { MongoMemoryReplSetManager } from './src/shared/e2e-test.helper';

afterAll(async () => {
  (await MongoMemoryReplSetManager.getInstance()).stop();
});
