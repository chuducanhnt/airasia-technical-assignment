require("dotenv").config();
import { INestApplication, ValidationPipe } from "@nestjs/common";
import { NestFactory } from "@nestjs/core";
import config from "config";
import * as express from "express";
import * as httpContext from "express-http-context";
import { createLightship } from "lightship";
import { Logger } from "nestjs-pino";
import responseTime from "response-time";
import { v4 as uuidV4 } from "uuid";
import { HttpExceptionFilter } from "./filters/http-exception.filter";
import { AppModule } from "./modules/app/app.module";
import { initializeSwagger } from "./shared/swagger.helper";

export async function bootstrap() {
    const app = await NestFactory.create(AppModule, {
        cors: true,
        logger: console,
    });
    const logger = app.get(Logger);

    app.useLogger(logger);

    await initializeApp(app, logger);
    initializeSwagger(app);

    const lightship = await initializeLightship(app);

    const port = config.get<number>("server.port");
    await app.listen(port);
    logger.log(`Start server on : ${port}`);
    lightship.signalReady();
}

async function initializeApp(app: INestApplication, logger: Logger) {
    app.use(express.urlencoded({ extended: true }));
    app.use(express.json());
    app.use(responseTime({ header: "x-response-time" }));
    app.use((req: express.Request, res: express.Response, next: () => void) => {
        const correlationId = uuidV4();
        httpContext.set("timestamp", Date.now());
        httpContext.set("correlationId", correlationId);
        req = Object.assign(req, {
            id: correlationId,
        });
        next();
    });
    app.useGlobalFilters(new HttpExceptionFilter(logger));
    app.useGlobalPipes(
        new ValidationPipe({
            transform: true,
            validationError: {
                target: false,
                value: false,
            },
            whitelist: true,
            transformOptions: {
                enableImplicitConversion: true,
            },
        }),
    );
    app.setGlobalPrefix(config.get("service.baseUrl"));
}

async function initializeLightship(app: INestApplication) {
    const lightship = await createLightship();

    lightship.registerShutdownHandler(async () => {
        await app.close();
    });

    return lightship;
}

/* eslint no-console: 0 */
bootstrap()
    .then(() => {
        console.log("Bootstrapped ...");
    })
    .catch((e) => {
        console.error("Error on bootsrapping the app ...", e);
    });
