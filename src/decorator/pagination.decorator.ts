import { BadRequestException, createParamDecorator, ExecutionContext } from "@nestjs/common";
import { plainToInstance } from "class-transformer";
import { isNumber, validateOrReject } from "class-validator";
import { PrePagination } from "src/shared/types";

export const Pagination = createParamDecorator(async (data: unknown, ctx: ExecutionContext) => {
    const request = ctx.switchToHttp().getRequest();
    const rawQuery = request.query;
    const query = plainToInstance(PrePagination, rawQuery);
    try {
        await validateOrReject(query);
    } catch (err) {
        throw new BadRequestException(err);
    }
    const page = isNumber(query.page) ? Number(query.page) : 0;
    const perPage = isNumber(query.perPage) ? Number(query.perPage) : 50;
    return {
        limit: perPage,
        offset: perPage * page,
    };
});
