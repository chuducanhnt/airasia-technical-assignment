import { INestApplication } from "@nestjs/common";
import { Test, TestingModuleBuilder } from "@nestjs/testing";
import { randomUUID } from "crypto";
import { MongoMemoryReplSet } from "mongodb-memory-server";
import { Logger } from "nestjs-pino";
import responseTime from "response-time";
import { HttpExceptionFilter } from "src/filters/http-exception.filter";
import supertest from "supertest";
import { promisify } from "util";
import { AppModule } from "../modules/app/app.module";
import { MongooseModuleConfigService } from "./mongodb.helper";

export class MongoMemoryReplSetManager {
    private static replicasInstance: MongoMemoryReplSet;
    private static semaphorePromise: Promise<unknown>;

    static async getInstance(): Promise<MongoMemoryReplSet> {
        if (!MongoMemoryReplSetManager.replicasInstance && !MongoMemoryReplSetManager.semaphorePromise) {
            let resolveSemaphore: (value: unknown) => void;
            MongoMemoryReplSetManager.semaphorePromise = new Promise((resolve) => {
                resolveSemaphore = resolve;
            });
            MongoMemoryReplSetManager.replicasInstance = await MongoMemoryReplSet.create({
                replSet: {
                    storageEngine: "wiredTiger",
                },
            });
            resolveSemaphore(true);
        }
        if (MongoMemoryReplSetManager.semaphorePromise) {
            await MongoMemoryReplSetManager.semaphorePromise;
        }
        return MongoMemoryReplSetManager.replicasInstance;
    }
}

export const initTestApp = async (
    overrides?: (testModule: TestingModuleBuilder) => TestingModuleBuilder,
): Promise<[INestApplication, MongoMemoryReplSet]> => {
    const replSet = await MongoMemoryReplSetManager.getInstance();

    const dbName = randomUUID();

    const dbUri = replSet.getUri(dbName);

    jest.spyOn(MongooseModuleConfigService.prototype, "createMongooseOptions").mockReturnValueOnce({
        uri: dbUri,
        dbName,
        directConnection: true,
    });

    let testBuilder = Test.createTestingModule({
        imports: [AppModule],
    });

    if (overrides) {
        testBuilder = overrides(testBuilder);
    }

    const fixture = await testBuilder.compile();

    const app = fixture.createNestApplication();
    app.use(responseTime({ header: "x-response-time" }));

    const logger = app.get(Logger);
    app.useGlobalFilters(new HttpExceptionFilter(logger));
    
    await app.init();
    return [app, replSet];
};

export const wait = promisify(setTimeout);

export const createRequestFunction = (app: INestApplication) =>
    async function request(
        url: string,
        {
            expected = 200,
            method = "get",
            body,
            contentType = "application/json",
            accept = "application/json",
            attachment,
            query,
            mockToken = "mock-token",
        }: {
            expected?: number;
            method?: "get" | "post" | "put" | "delete";
            body?: any;
            contentType?: string;
            accept?: string;
            attachment?: {
                name: string;
                file: string;
            };
            query?: Record<string, any>;
            mockToken?: string;
        } = {},
    ) {
        const agent = supertest.agent(app.getHttpServer());
        const req = agent[method](url).set("Accept", accept).set("sub", "sub").set("access-token", mockToken);
        if (attachment) {
            req.attach(attachment.name, attachment.file);
        }
        if (query) {
            req.query(query);
        }
        const reqAfterSend = body ? req.set("Content-Type", contentType).send(body) : req;
        return reqAfterSend.expect(expected).then((res) => res);
    };
