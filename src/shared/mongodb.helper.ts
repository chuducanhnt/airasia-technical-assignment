import { MongooseModuleOptions, MongooseOptionsFactory } from "@nestjs/mongoose";
import config from "config";

const mongoUri = config.get<string>("mongodb.uri");
const dbName = config.get<string>("mongodb.dbName");

export class MongooseModuleConfigService implements MongooseOptionsFactory {
    createMongooseOptions(): MongooseModuleOptions | Promise<MongooseModuleOptions> {
        return {
            uri: mongoUri,
            dbName,
        }
    }
    
}