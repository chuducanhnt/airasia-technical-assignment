import { INestApplication } from "@nestjs/common";
import { DocumentBuilder, SwaggerModule } from "@nestjs/swagger";
import config from "config";

const PROJECT_NAME = config.get<string>("service.name");
const PROJECT_VERSION = config.get<string>("service.appVersion");
const SWAGGER_PATH = config.get<string>("service.docsBaseUrl");

export const initializeSwagger = (app: INestApplication) => {
    const options = new DocumentBuilder().addBearerAuth().setTitle(PROJECT_NAME).setVersion(PROJECT_VERSION).build();
    const document = SwaggerModule.createDocument(app, options);
    SwaggerModule.setup(SWAGGER_PATH, app, document, {
        customSiteTitle: PROJECT_NAME,
        swaggerOptions: {
            defaultModelsExpandDepth: -1,
            displayRequestDuration: true,
            docExpansion: "none",
            filter: true,
        },
    });
};
