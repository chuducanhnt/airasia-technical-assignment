import { ApiProperty, ApiPropertyOptional } from "@nestjs/swagger";
import { Type } from "class-transformer";
import { IsNumber, IsOptional, Max, Min } from "class-validator";

export class PrePagination {
    @ApiPropertyOptional({
        type: String,
    })
    @Type(() => Number)
    @IsNumber()
    @IsOptional()
    @Min(0)
    page: number;

    @ApiPropertyOptional({
        type: String,
    })
    @IsNumber()
    @Type(() => Number)
    @IsOptional()
    @Min(1)
    @Max(50)
    perPage: number;
}

export class IPostPagination {
    limit: number;
    offset: number;
}

export class PaginationResponse<T> {
    @ApiProperty({
        type: Number,
    })
    total: number;

    data: T[];
}
