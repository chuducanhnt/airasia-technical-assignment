import { Module } from "@nestjs/common";
import { MongooseModule } from "@nestjs/mongoose";
import { LoggerModule } from "nestjs-pino";
import { UserModule } from "src/modules/user/user.module";
import { MongooseModuleConfigService } from "src/shared/mongodb.helper";

@Module({
    imports: [MongooseModule.forRootAsync({ useClass: MongooseModuleConfigService }), UserModule, LoggerModule.forRoot()],
})
export class AppModule {}
