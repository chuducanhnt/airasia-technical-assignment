import { HttpStatus, INestApplication } from "@nestjs/common";
import { getModelToken } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { createRequestFunction, initTestApp } from "src/shared/e2e-test.helper";
import { UserEntity, USER_MODEL } from "../user.entity";
import { UserStatus } from "../user.types";

describe("UserModule e2e", () => {
    let app: INestApplication;
    let request: ReturnType<typeof createRequestFunction>;
    let userModel: Model<UserEntity>;

    beforeAll(async () => {
        [app] = await initTestApp();
        request = createRequestFunction(app);
        userModel = app.get(getModelToken(USER_MODEL));
    });

    afterAll(async () => {
        await Promise.allSettled([app?.close()]);
    });

    beforeEach(async () => {
        await userModel.deleteMany({});
    });

    const createUserBody = {
        userId: "duc-anh",
        fullName: "Duc Anh",
        phoneNumber: "0936609206",
        status: "ACTIVE",
    };

    describe("create user", () => {
        it("should create user", async () => {
            const createUserResponse = await request("/users/", {
                method: "post",
                expected: HttpStatus.CREATED,
                body: createUserBody,
            });
            expect(createUserResponse.body).toEqual({
                id: expect.any(String),
                userId: "duc-anh",
                fullName: "Duc Anh",
                phoneNumber: "0936609206",
                status: "ACTIVE",
            });
            expect(await userModel.estimatedDocumentCount({})).toEqual(1);
        });

        it("should throw bad request if don send body", async () => {
            await request("/users/", {
                method: "post",
                expected: HttpStatus.BAD_REQUEST,
            });
        });
    });

    describe("Update user", () => {
        it("should throw bad request if not valid userId", async () => {
            const updateUserResponse = await request("/users/123123", {
                method: "put",
                expected: HttpStatus.BAD_REQUEST,
                body: { status: UserStatus.DISABLE },
            });
            expect(updateUserResponse.body).toEqual({
                statusCode: 400,
                message: "123123 not is a objectId",
                error: "Bad Request",
            });
        });
        it("should throw bad request if not fould user", async () => {
            const updateUserResponse = await request("/users/507f1f77bcf86cd799439011", {
                method: "put",
                expected: HttpStatus.BAD_REQUEST,
                body: { status: UserStatus.DISABLE },
            });
            expect(updateUserResponse.body).toEqual({
                statusCode: 400,
                message: "User not found",
                error: "Bad Request",
            });
        });
        it("should update some user field", async () => {
            const createUserResponse = await request("/users/", {
                method: "post",
                expected: HttpStatus.CREATED,
                body: createUserBody,
            });
            const id = createUserResponse.body.id;
            await request(`/users/${id}`, {
                method: "put",
                expected: HttpStatus.OK,
                body: { status: UserStatus.DISABLE },
            });
            const user = await userModel.findById(id);
            expect(await userModel.estimatedDocumentCount({})).toEqual(1);
            expect(user.toJSON()).toEqual({
                _id: expect.any(Object),
                userId: "duc-anh",
                fullName: "Duc Anh",
                phoneNumber: "0936609206",
                status: "DISABLE",
                createdAt: expect.any(Date),
                updatedAt: expect.any(Date),
                __v: 0,
            });
        });
    });

    describe("Get user by id", () => {
        it("should get user by id correctly", async () => {
            const createUserResponse = await request("/users/", {
                method: "post",
                expected: HttpStatus.CREATED,
                body: createUserBody,
            });
            const id = createUserResponse.body.id;
            const getUserResponse = await request(`/users/${id}`, {
                method: "get",
                expected: HttpStatus.OK,
            });
            expect(getUserResponse.body).toEqual({ ...createUserBody, id });
        });
        it("should throw bad request if not valid userId", async () => {
            const getUserResponse = await request("/users/123123", {
                method: "get",
                expected: HttpStatus.BAD_REQUEST,
            });
            expect(getUserResponse.body).toEqual({
                statusCode: 400,
                message: "123123 not is a objectId",
                error: "Bad Request",
            });
        });
    });

    describe("Index user", () => {
        it("if dont send page,perPage then get default item", async () => {
            for (let i = 0; i < 10; i++) {
                await request("/users", {
                    method: "post",
                    expected: HttpStatus.CREATED,
                    body: createUserBody,
                });
            }
            const indexResponse = await request("/users/index", {
                method: "get",
                expected: HttpStatus.OK,
            });
            expect(indexResponse.body.total).toEqual(10);
            expect(indexResponse.body.data.length).toEqual(10);
        });

        it("send page and perPage", async () => {
            for (let i = 0; i < 10; i++) {
                await request("/users", {
                    method: "post",
                    expected: HttpStatus.CREATED,
                    body: createUserBody,
                });
            }
            const indexResponse = await request("/users/index", {
                method: "get",
                expected: HttpStatus.OK,
                query: {
                    page: 2,
                    perPage: 4,
                },
            });
            expect(indexResponse.body.total).toEqual(10);
            expect(indexResponse.body.data.length).toEqual(2);
        });

        it("send negative page and perPage", async () => {
            for (let i = 0; i < 10; i++) {
                await request("/users", {
                    method: "post",
                    expected: HttpStatus.CREATED,
                    body: createUserBody,
                });
            }
            const indexResponse = await request("/users/index", {
                method: "get",
                expected: HttpStatus.BAD_REQUEST,
                query: {
                    page: -1,
                    perPage: 4,
                },
            });
        });
    });

    describe("Delete user by id", () => {
        it("should delete user by id correctly", async () => {
            const createUserResponse = await request("/users/", {
                method: "post",
                expected: HttpStatus.CREATED,
                body: createUserBody,
            });
            const id = createUserResponse.body.id;
            await request(`/users/${id}`, {
                method: "delete",
                expected: HttpStatus.OK,
                body: { status: UserStatus.DISABLE },
            });
            expect(await userModel.estimatedDocumentCount({})).toEqual(0);
        });
        it("should throw bad request if not valid userId", async () => {
            const deleteUserResponse = await request("/users/123123", {
                method: "get",
                expected: HttpStatus.BAD_REQUEST,
            });
            expect(deleteUserResponse.body).toEqual({
                statusCode: 400,
                message: "123123 not is a objectId",
                error: "Bad Request",
            });
        });
    });
});
