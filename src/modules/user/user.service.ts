import { BadRequestException, Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { isObjectIdOrHexString, Model } from "mongoose";
import { IPostPagination, PaginationResponse } from "src/shared/types";
import { CreateUserDto } from "./dto/create-user.dto";
import { UpdateUserDto } from "./dto/update-user.dto";
import { UserResponseDto } from "./dto/user.response.dto";
import { UserDocument, USER_MODEL } from "./user.entity";
import { convertUserDocument2UserResponseDto } from "./user.helper";

@Injectable()
export class UserService {
    constructor(@InjectModel(USER_MODEL) private readonly userModel: Model<UserDocument>) {}

    public async createUser(createrUserDto: CreateUserDto) {
        if (Object.keys(createrUserDto).length === 0) {
            // Class validator miss this case. Because it only check if have the body
            throw new BadRequestException("Object id emtpty");
        }
        const userDocument = await this.userModel.create(createrUserDto);
        return convertUserDocument2UserResponseDto(userDocument);
    }

    public async updateUserById(id: string, updateUserDto: UpdateUserDto) {
        await this.checkUserExistedById(id);
        const user: UserDocument = await this.userModel
            .findOneAndUpdate<UserDocument>(
                { id },
                {
                    $set: updateUserDto,
                },
                {
                    new: true,
                },
            )
            .exec();
        return convertUserDocument2UserResponseDto(user);
    }

    public async deleteUserById(id: string) {
        await this.checkUserExistedById(id);
        return this.userModel.deleteOne({ id });
    }

    public async getUserById(id: string) {
        if (!isObjectIdOrHexString(id)) {
            throw new BadRequestException(`${id} not is a objectId`);
        }
        const user = await this.userModel.findById(id);
        return convertUserDocument2UserResponseDto(user);
    }

    public async countAllUsers(): Promise<number> {
        return this.userModel.estimatedDocumentCount();
    }

    public async indexUsers(pagination: IPostPagination): Promise<PaginationResponse<UserResponseDto>> {
        const [users, total] = await Promise.all([
            this.userModel.find({}).skip(pagination.offset).limit(pagination.limit).sort({ _id: -1 }).exec(),
            this.countAllUsers(),
        ]);
        return {
            total,
            data: users.map((user) => convertUserDocument2UserResponseDto(user)),
        };
    }

    private async checkUserExistedById(id: string) {
        if (!isObjectIdOrHexString(id)) {
            throw new BadRequestException(`${id} not is a objectId`);
        }
        const countUser = await this.userModel.countDocuments({
            id,
        });
        if (!countUser) {
            throw new BadRequestException(`User not found`);
        }
    }
}
