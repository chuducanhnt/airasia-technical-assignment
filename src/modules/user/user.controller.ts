import { Body, Controller, Delete, Get, HttpStatus, Param, Post, Put } from "@nestjs/common";
import { ApiExtraModels, ApiOperation, ApiQuery, ApiResponse, ApiTags } from "@nestjs/swagger";
import { IsDefined } from "class-validator";
import { ApiOkResponsePagination } from "src/decorator/api-ok-response-pagination.decorator";
import { Pagination } from "src/decorator/pagination.decorator";
import { IPostPagination, PaginationResponse, PrePagination } from "src/shared/types";
import { CreateUserDto } from "./dto/create-user.dto";
import { UpdateUserDto } from "./dto/update-user.dto";
import { UserResponseDto } from "./dto/user.response.dto";
import { UserService } from "./user.service";

@Controller("users")
@ApiTags("users")
export class UserController {
    constructor(private readonly userService: UserService) {}

    @Post("")
    @ApiOperation({
        operationId: "createUser",
    })
    @ApiResponse({
        status: HttpStatus.CREATED,
        type: UserResponseDto,
        description: "Created user succeed",
    })
    public async createUser(@Body() createUserDto: CreateUserDto): Promise<UserResponseDto> {
        return this.userService.createUser(createUserDto);
    }

    @Get("/index")
    @ApiOperation({
        operationId: "indexUser",
    })
    @ApiExtraModels(PaginationResponse, UserResponseDto)
    @ApiOkResponsePagination(UserResponseDto)
    @ApiQuery({
        type: PrePagination,
    })
    public async indexUsers(@Pagination() pagination: IPostPagination) {
        return this.userService.indexUsers(pagination);
    }

    @Get(":id")
    @ApiOperation({
        operationId: "getUserByid",
    })
    @ApiResponse({
        status: HttpStatus.OK,
        type: UserResponseDto,
        description: "Get user succeed",
    })
    public async getUserById(@Param("id") id: string) {
        return this.userService.getUserById(id);
    }

    @Put(":id")
    @ApiOperation({
        operationId: "updateUserById",
    })
    @ApiResponse({
        status: HttpStatus.OK,
        type: UserResponseDto,
        description: "Update user succeed",
    })
    public async updateUser(@Param("id") id: string, @Body() updateUserDto: UpdateUserDto): Promise<UserResponseDto> {
        return this.userService.updateUserById(id, updateUserDto);
    }

    @Delete(":id")
    @ApiOperation({
        operationId: "deleteUserById",
    })
    @ApiResponse({
        status: HttpStatus.OK,
        type: UserResponseDto,
        description: "Delete user succeed",
    })
    public async deleteUser(@Param("id") id: string) {
        return this.userService.deleteUserById(id);
    }
}
