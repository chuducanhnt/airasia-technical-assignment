import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import { HydratedDocument } from "mongoose";
import { UserStatus } from "./user.types";

export const USER_MODEL = "users";

@Schema({
    timestamps: true,
    collection: USER_MODEL,
    collation: { locale: "vi" },
})
export class UserEntity {
    @Prop({
        type: String,
        required: true,
    })
    userId: string;

    @Prop({
        type: String,
        required: true,
    })
    fullName: string;

    @Prop({
        type: String,
        required: true,
    })
    phoneNumber: string;

    @Prop({
        type: String,
        required: true,
    })
    status: UserStatus;
}

export type UserDocument = HydratedDocument<UserEntity>;

export const UserSchema = SchemaFactory.createForClass(UserEntity);