import { ApiProperty } from "@nestjs/swagger";
import { IsDefined, IsEnum, IsNotEmpty, IsString } from "class-validator";
import { UserStatus } from "../user.types";

export class CreateUserDto {
    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    @IsDefined()
    userId: string;

    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    @IsDefined()
    fullName: string;

    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    @IsDefined()
    phoneNumber: string;

    @ApiProperty({
        enum: Object.values(UserStatus),
    })
    @IsEnum(UserStatus)
    @IsNotEmpty()
    @IsDefined()
    status: UserStatus;
}
