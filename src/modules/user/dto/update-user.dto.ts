import { ApiProperty } from "@nestjs/swagger";
import { IsEnum, IsOptional, IsString } from "class-validator";
import { UserStatus } from "../user.types";

export class UpdateUserDto {
    @ApiProperty()
    @IsString()
    @IsOptional()
    userId?: string;

    @ApiProperty()
    @IsString()
    @IsOptional()
    fullName?: string;

    @ApiProperty()
    @IsString()
    @IsOptional()
    phoneNumber?: string;

    @ApiProperty({
        enum: Object.values(UserStatus),
    })
    @IsEnum(UserStatus)
    @IsOptional()
    status?: UserStatus;
}