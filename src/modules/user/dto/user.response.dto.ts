import { ApiProperty } from "@nestjs/swagger";
import { UserStatus } from "../user.types";

export class UserResponseDto {
    @ApiProperty()
    id: string;

    @ApiProperty()
    userId: string;

    @ApiProperty()
    fullName: string;

    @ApiProperty()
    phoneNumber: string;

    @ApiProperty({
        enum: Object.values(UserStatus),
    })
    status: UserStatus;
}
