import { UserResponseDto } from "./dto/user.response.dto";
import { UserDocument } from "./user.entity";

export const convertUserDocument2UserResponseDto = (userDocument: UserDocument): UserResponseDto => {
    return {
        id: userDocument.id,
        userId: userDocument.userId,
        fullName: userDocument.fullName,
        phoneNumber: userDocument.phoneNumber,
        status: userDocument.status,
    };
};
