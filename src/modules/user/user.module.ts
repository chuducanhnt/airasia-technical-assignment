import { Module } from "@nestjs/common";
import { MongooseModule } from "@nestjs/mongoose";
import { UserController } from "./user.controller";
import { UserSchema, USER_MODEL } from "./user.entity";
import { UserService } from "./user.service";

@Module({
    imports: [MongooseModule.forFeature([{ name: USER_MODEL, schema: UserSchema }])],
    controllers: [UserController],
    providers: [UserService],
})
export class UserModule {}
