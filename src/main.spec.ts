import { NestFactory } from '@nestjs/core';
import { initTestApp, wait } from './shared/e2e-test.helper';

jest.setTimeout(60000);

describe('main.ts', () => {
  it('Should start ok', async () => {
    const [app] = await initTestApp();
    jest.spyOn(app, 'listen').mockImplementation(jest.fn());
    jest.spyOn(NestFactory, 'create').mockReturnValue(Promise.resolve(app));
    require("./main");
    await wait(10000);
    expect(app.listen).toBeCalled();
    await app.close();
  });
});
