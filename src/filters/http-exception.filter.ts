import { ArgumentsHost, Catch, HttpException } from "@nestjs/common";
import { BaseExceptionFilter } from "@nestjs/core";
import { Logger } from "nestjs-pino";

@Catch()
export class HttpExceptionFilter extends BaseExceptionFilter {
    constructor(private readonly logger: Logger) {
        super();
    }

    catch(error: HttpException, host: ArgumentsHost) {
        const response = host.switchToHttp().getResponse();

        this.logger.error(error, error.stack, "Http Error");

        response.status(error.getStatus()).json(error.getResponse());
    }
}
