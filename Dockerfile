#
# ---- Build ----
FROM node:16.18-alpine3.15 AS build
ARG WORK_DIR=/var/www/node
WORKDIR ${WORK_DIR}

COPY . .
RUN npm ci
RUN npm run build
# prune non-production node packages
RUN npm prune --production
RUN wget https://gobinaries.com/tj/node-prune && sh node-prune && node-prune

# ---- Release ----
FROM node:14.20.1-alpine AS release
ARG WORK_DIR=/var/www/node
WORKDIR ${WORK_DIR}
COPY --from=build ${WORK_DIR}/node_modules ./node_modules
COPY --from=build ${WORK_DIR}/dist ./dist
COPY --from=build ${WORK_DIR}/config ./config
COPY swagger*.json package*.json ./
CMD npm run start:prod
